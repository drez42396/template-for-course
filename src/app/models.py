from django.db import models
from app.internal.models.admin_user import AdminUser


class Person(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30, null=True)
    phone = models.CharField(max_length=10, blank=True)
    telegram_id = models.TextField(unique=True)

    def __str__(self):
        return self.first_name

    def json(self):
        return {attr: getattr(self, attr) for attr in ["first_name", "last_name", "phone", "telegram_id"]}

    class Meta:
        verbose_name = "Person"
        verbose_name_plural = "People"
