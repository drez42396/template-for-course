import json

import requests
import telebot
from decouple import config

TELEGRAM_TOKEN = config("TELEGRAM_TOKEN")


bot = telebot.TeleBot(TELEGRAM_TOKEN)
host = config("HOST")


@bot.message_handler(commands=["start"])
def send_welcome(message):

    """
    По команде старт считываем информацию из профиля пользователя.

    С этой информацией отправляем пост запрос на регистрацию пользователя.
    При успешном запросе просим номер телефона, чтобы сразу его записать в базу. Если пользователь ввел корректный номер - 
    записываем в базу и cообщаем об успехе.
    Вслучае ошибки при записи номера или при ошибках в валидации номера просим ввести номер позже
    """

    user = {
        "telegram_id": message.from_user.id,
        "first_name": message.from_user.first_name,
        "last_name": message.from_user.last_name,
    }
    bot.send_message(message.chat.id, f"Начинаю регистрацию...")

    response = requests.post(url=f"{host}/registration/", data=json.dumps(user))
    if response.status_code == 201:
        bot.send_message(message.chat.id, "Для завершения регистрации укажите номер телефона в 10-ти значном формате")

        @bot.message_handler(content_types=["text"])
        def get_phone(message):
            if all([message.text.isdigit(), len(message.text) == 10]):

                body = {"telegram_id": message.from_user.id, "phone": message.text}
                response = requests.post(url=f"{host}/update/", data=json.dumps(body))
                result_message = "Регистрация завершена" if response.status_code == 200 else "Что-то пошло нет, попробуй позже. Для ввожа номера скажи команду /sendPhone"
                bot.send_message(message.chat.id, result_message)

            else:

                bot.send_message(
                    message.chat.id,
                    "Номер не корректный. Когда будете готовы сообщить номер введите команду: /sendPhone",
                )

    else:
        response_code = requests.get(url=f"{host}/me/", data=json.dumps(user)).status_code
        result = {
            403: f"Привет, {message.from_user.first_name}! Ты уже зарегистрирован, но у тебя не указан номер телефона. Чтобы добавить номер,введи комманду: /sendPhone",
            404: f"Для регистрации заполни профиль и повтори команду",
            200: f"Привет, {message.from_user.first_name}! Ты уже зарегистрирован!",
        }
        bot.send_message(message.chat.id, result.get(response_code, "Произошла ошибка, попробуй позже"))


@bot.message_handler(commands=["sendPhone"])
def send_phone(message):

    """
    Просим пользователя ввести номер телефона.
    Валидируем номер. Если все в порядке, сообщаем об успехе.
    Если номер невалидный, просим пользователя повторить команду позже
    """

    bot.send_message(message.chat.id, "Введите номер телефона в 10-ти значном формате")

    @bot.message_handler(content_types=["text"])
    def get_phone(message):
        if all([message.text.isdigit(), len(message.text) == 10]):

            body = {"telegram_id": message.from_user.id, "phone": message.text}
            requests.post(url=f"{host}/update/", data=json.dumps(body))
            bot.send_message(message.chat.id, f"Спасибо, {message.from_user.first_name}, я тебе позвоню!")

        else:
            bot.send_message(
                message.chat.id, "Номер не корректный. Когда будете готовы сообщить номер, повторите команду"
            )


@bot.message_handler(commands=["me"])
def me_info(message):

    """
    Отправляем запрос на получение данных о пользователе, возвращаем ответ сервера
    """
    
    body = {"telegram_id": message.from_user.id}
    response = requests.get(url=f"{host}/me/", data=json.dumps(body))
    for k, v in response.json().items():
        bot.send_message(message.chat.id, f"{k}: {v}")


@bot.message_handler(commands=["help"])
def help_message(message):

    commands_list = """
    /start - регистрация
    /me - информация о зарегистрированом пользователе
    /help - помощь
    """
    bot.send_message(message.chat.id, commands_list)


bot.polling(none_stop=True)