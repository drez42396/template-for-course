import json

from django.http import HttpResponse

from app.models import Person


def add_person(**kwargs):

    """
    Создаем новогопользователя в базе.
    В случае успеха возвращаем код 201. Если возникнет ошибка - вернем код 400
    """
    
    try:
        Person.objects.create(**kwargs)
        status_code = 201
    except:
        status_code = 400
    finally:
        return status_code


def update_person(**kwargs):

    """
    Ищем пользователя для обновления по уникальному ключу, если пользователь не найден возвращаем 404
    если найден, обновляем переданные поля. в случае успеха отдаем код 200, если не смогли обновить, возвращаем 400
    """

    try:
        Person.objects.filter(telegram_id=kwargs.get("telegram_id")).update(**kwargs)
        status_code = 200
    except Person.DoesNotExist:
        status_code = 404
    except Exception:
        status_code = 400
    finally:
        return status_code


def registration_person(request):

    """
    Запрос на регистрацию принимаем только с методом POST
    Добавляем пользователя по данным из тела запроса, возвращаем статус код результата добавления пользователя в базу
    """

    if request.method == "POST":
        person_data = json.loads(request.body)
        status_code = add_person(**person_data)
        response = HttpResponse()
        response.status_code = status_code
    else:

        body = {"error": f"Метод {request.method} не поддерживается для этого запроса"}
        response = HttpResponse(json.dumps(body), headers={"Content-Type": "application/json"})
        response.status_code = 405

    return response


def get_permissions(telegram_id):

    """
    Проверяем, что у пользователя указан номер телефона
    возвращаем в результате словарь с булевским флагом наличия/отсутствия разрешения и ответом на http запрос
    """

    try:
        pers = Person.objects.get(telegram_id=telegram_id)
        phone = pers.json().get("phone")
        if not phone:
            body = {"error": "Метод не доступен, укажите номер телефона"}
            response = HttpResponse(json.dumps(body), headers={"Content-Type": "application/json"})
            response.status_code = 403
            permissions = False
        else:
            permissions = True
            pers = Person.objects.get(telegram_id=telegram_id)
            response = HttpResponse(json.dumps(pers.json()), headers={"Content-Type": "application/json"})

    except:
        response = HttpResponse()
        response.status_code = 404
        body = {"error": "Пользователь не найден"}
        permissions = False
        response(json.dumps(body), headers={"Content-Type": "application/json"})

    finally:
        return {"permissions": permissions, "response": response}


def get_person(request):

    """
    проверяем, есть ли разрешение у пользователя на использование метода и отдаем соответствующий ответ
    """
    
    telegram_id = json.loads(request.body).get("telegram_id")
    perm, resp = get_permissions(telegram_id).values()
    return resp


def request_update(request):

    """
    принимаем только POST запрос
    проверяем разрешения. Если их нет, то даем указать только номер телефона
    если есть, запрос выполняется успешно
    """

    try:
        if request.method == "POST":

            person_data = json.loads(request.body)
            perm, resp = get_permissions(person_data.get("telegram_id")).values()
            print(perm, person_data.get("phone"), resp)
            if all([not perm, not person_data.get("phone")]):
                return resp
            else:
                status_code = update_person(**person_data)
                response = HttpResponse()
                response.status_code = status_code
                return response
        else:
            body = {"error": "Метод GET не поддерживается для этого запроса"}
            response = HttpResponse(json.dumps(body), headers={"Content-Type": "application/json"})
            response.status_code = 405
            return response
    except:
        response = HttpResponse()
        response.status_code = 400
        return response
