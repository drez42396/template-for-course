from django.urls import path

from . import views

urlpatterns = [
    path("registration/", views.registration_person, name="registration"),
    path("me/", views.get_person, name="me"),
    path("update/", views.request_update, name="update"),
]
