from django.contrib import admin

from .models import Person

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"


class PersonAdmin(admin.ModelAdmin):
    list_display = ("pk", "telegram_id", "first_name", "last_name", "phone")


admin.site.register(Person, PersonAdmin)
