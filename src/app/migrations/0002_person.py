# Generated by Django 4.0.2 on 2022-02-08 06:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("app", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="Person",
            fields=[
                ("id", models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                ("first_name", models.CharField(max_length=30)),
                ("last_name", models.CharField(max_length=30)),
                ("phone", models.CharField(blank=True, max_length=10)),
                ("telegram_id", models.TextField(unique=True)),
                ("email", models.EmailField(max_length=254)),
                ("date_of_birth", models.DateField()),
            ],
            options={
                "verbose_name": "Person",
                "verbose_name_plural": "People",
            },
        ),
    ]
